## Android实用工具合集-ATools

- ### 分为两个版本
     kotlin  
        `implementation 'com.gushenge.atools:kotlin:0.0.5 `  
     java  
        `implementation 'com.gushenge.atools:java:0.0.5'`
        
- ### 说明
    APP的targetSdkVersion应大于23  
    两个仓库的targetSdkVersion都为29
    

- ### [具体功能](https://github.com/Gushenge/Atools/wiki)
- 附：
所有已知仓库路径
        
	    url = https://github.com/Gushenge/Atools.git
	    url = https://git.dev.tencent.com/Gushenge/Atools.git
	    url = https://gitee.com/gzyyu/Atools.git